#include <DHT.h>
#include "etherShield.h"
#include "ETHER_28J60.h"


//Podłączenia
//ETHER_28J60
//
//czujniki
//przekaźnik lampy pin 7, przekaźnik wentylacji pin 8
//czujnik temperatury pin A0
 
//Digital
//czujnik wilgotności
 #define DHTPIN 2 
 #define DHTTYPE DHT11
 
 //globalne
int Lampa=7;
int Wen=8;
int button=3;
int Czujka=4;
//analog
int TemS=A1;
int Dioda=A2;

int sensorValue = 0;          // the sensor value
//reszta
bool FlagaLampa;
int i=0;
int f=0;
float temperatureC = 0;       //Odczyt temperatury
float temperatureCSr = 0;     //srednia
float temperatureCSum = 0;    //suma do sredniej

unsigned long TempTime = millis() + 10000;  // +10s
unsigned long TempDioda = millis() + 10;
 
static uint8_t mac[6] = {0x54, 0x55, 0x58, 0x10, 0x00, 0x24};   
static uint8_t ip[4] = {192, 168, 1, 15};                       
static uint16_t port = 80;                                     
 
ETHER_28J60 e;
DHT dht(DHTPIN, DHTTYPE);
 
void setup()
{ 
  Serial.begin(9600);
  
  pinMode(Lampa, OUTPUT);
  digitalWrite(Lampa, LOW);

  pinMode(Wen, OUTPUT);
  digitalWrite(Wen, LOW);
 
  pinMode(button, INPUT);
  digitalWrite(button, HIGH);

  pinMode(Czujka, INPUT);

  dht.begin();  //inicjalizacja DHT

  temperatureC = -100;
 
  delay(1000);
  e.setup(mac, ip, port);
}
 
void loop()
{ 
  char* params;
  float Wilgotnosc = dht.readHumidity();

  //--------czas + 10s----------
  if(millis() >= TempTime)
    {
      sensorValue = analogRead(TemS);
      
      float voltage = sensorValue * 5;
      
      //voltage /= 1024.0;
      temperatureC = (5.0 * sensorValue * 10000.0)/1024.0;//(voltage/1024.0 - 0.5) * 10000;
      if(isnan(Wilgotnosc)){
        Serial.println("Błąd odczytu z czujnika");
      }
      else {
        Serial.print("Wilgotnosc= ");
        Serial.print(Wilgotnosc);
      }
      TempTime = millis() + 1000;
      
      //liczenie sredniej
      if(i <= 50){
        i++;
        temperatureCSum += temperatureC;
        temperatureCSr = temperatureCSum / i;
      }
      else{
        i = 0;
        temperatureCSr = 0;
        temperatureCSum = 0;
      }
      
      Serial.println(temperatureC); 
      Serial.println(temperatureCSr);
      Serial.println(sensorValue); 
    }//koniec pomairu temperatury
    if(FlagaLampa == 0 ){
      if (digitalRead(Czujka) == (LOW)) { // zapala diodę na pinie 13 jeśli wykryje ruch
              digitalWrite(Lampa, LOW);
        } 
        else {
            digitalWrite(Lampa, HIGH);
        }
    }



  //---------------------------------------------
  //          KOMUNIKACJA ETHERNET
  //---------------------------------------------
  if (params = e.serviceRequest())
  {
  //----------------światło---------------
  if (strcmp(params, "lampka_off") == 0) {
    FlagaLampa = 0;
    digitalWrite(Lampa, LOW);
    e.print("Light is turned off.");
  } 
  else if (strcmp(params, "lampka_on") == 0) {
    FlagaLampa = 1;
    digitalWrite(Lampa, HIGH);
    e.print("Light is turned on.");
  } 
  else if (strcmp(params, "statusL") == 0) {
    e.print(digitalRead(Lampa));
  }//------Koniec światło---
  //----------------Wentylacja--------------
  else if (strcmp(params, "wen_off") == 0) {
    digitalWrite(Wen, LOW);
    e.print("Wentylacja jest wyłączona.");
  } 
  else if (strcmp(params, "wen_on") == 0) {
    digitalWrite(Wen, HIGH);
    e.print("Wentylacja została włączona.");
  }
  else if (strcmp(params, "statusW") == 0) {
    e.print(digitalRead(Wen));
  }//------koniec wentylacja--------
  
  //------Temperatura-----
   else if (strcmp(params, "temp_in") == 0) {
      e.print(temperatureCSr);
   }//------Koniec temperatura---
  //------Wilgotnosc------
   else if (strcmp(params, "wilg") == 0) {
    if(isnan(Wilgotnosc)){
        e.print("-100");
      }
      else {
        e.print(Wilgotnosc);
      }
  }//------koniec wilgotnosci------
    else {
    e.print("Hello, World! :P");
    }
    e.respond();
  }//koniec ENC28J60


 //-------lampa na włącznik-------
  if(digitalRead(button) == LOW) {
    if(f == 0) { 
     f=1; 
    } else if(f == 1) { 
      digitalWrite(Lampa, LOW);
      f=0; 
    }
  }
 
}//koniec loop

void LampaON () {
  for (int j=0; i<=255; j++) {
    if(millis() >= TimeDioda){
        analogWtite(Dioda,i);
        TimeDioda=millis()+10;
    }
  }
}

 
